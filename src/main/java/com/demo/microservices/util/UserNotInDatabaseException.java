package com.demo.microservices.util;

public class UserNotInDatabaseException extends Exception {

    public UserNotInDatabaseException(String message) {
        super(message);
    }
}
