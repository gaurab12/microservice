package com.demo.microservices.util;


import com.demo.microservices.entities.ToDo;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ToDoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return ToDo.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ToDo toDo = (ToDo) target;

        String priority = toDo.getPriority();

        if (!"high".equals(priority) && !"low".equals(priority)) {
            errors.rejectValue("priority", "Priority must be 'high' or 'low'!");
        }

    }
}
