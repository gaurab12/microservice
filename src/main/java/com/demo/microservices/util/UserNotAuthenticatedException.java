package com.demo.microservices.util;

public class UserNotAuthenticatedException extends Exception{

    public UserNotAuthenticatedException(String message) {
        super(message);
    }
}
