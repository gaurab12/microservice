package com.demo.microservices.dao;

import com.demo.microservices.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDao extends JpaRepository<User, String> {

    // Name Strategy
    Optional<User> findUserByEmail(String email);
}
