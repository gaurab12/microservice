package com.demo.microservices.ToDoMicroservice;

import com.demo.microservices.dao.ToDoDao;
import com.demo.microservices.dao.UserDao;
import com.demo.microservices.entities.ToDo;
import com.demo.microservices.entities.User;
import com.demo.microservices.util.EncryptionUtils;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Date;

@SpringBootApplication
@EnableJpaRepositories("com.demo.microservices.dao")
@EntityScan("com.demo.microservices.entities")
@ComponentScan("com.demo")
@Log
public class ToDoMicroserviceApplication implements CommandLineRunner {

	@Autowired
	UserDao userDao;

	@Autowired
	ToDoDao toDoDao;

	@Autowired
	EncryptionUtils encryptionUtils;

	public static void main(String[] args) {
		SpringApplication.run(ToDoMicroserviceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		log.info("Fill H2 in-memory database");

		String encryptedPwd;
		encryptedPwd = encryptionUtils.encrypt("password1");
		userDao.save(new User("gaurabkharel@yahoo.com", "gaurab kharel", encryptedPwd));

		encryptedPwd = encryptionUtils.encrypt("password2");
		userDao.save(new User("sk@gmail.com", "Sanjay Khanal", encryptedPwd));

		encryptedPwd = encryptionUtils.encrypt("password3");
		userDao.save(new User("vp@gmail.com", "Vijay Pandey", encryptedPwd));

		toDoDao.save(new ToDo(1, "microservice demo", new Date(), "high",
				"gaurabkharel@yahoo.com"));
		toDoDao.save(new ToDo(null, "spring demo", null, "low",
				"gaurabkharel@yahoo.com"));

		toDoDao.save(new ToDo(3, "Have lunch", new Date(), "high", "sk@gmail.com"));
		toDoDao.save(new ToDo(null, "go shopping", null, "low", "sk@gmail.com"));

		toDoDao.save(new ToDo(5, "cook dinner", new Date(), "high", "vp@gmail.com"));
		toDoDao.save(new ToDo(null, "goto bed", null, "low", "vp@gmail.com"));


		log.info("Database operation completed");
	}
}
