package com.demo.microservices.controller;

import com.demo.microservices.entities.ToDo;
import com.demo.microservices.entities.User;
import com.demo.microservices.service.LoginService;
import com.demo.microservices.service.ToDoService;
import com.demo.microservices.util.JsonResponseBody;
import com.demo.microservices.util.ToDoValidator;
import com.demo.microservices.util.UserNotAuthenticatedException;
import com.demo.microservices.util.UserNotInDatabaseException;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
public class Controller {

    @Autowired
    LoginService loginService;

    @Autowired
    ToDoService toDoService;

    @PostMapping(value = "/login")
    public ResponseEntity<JsonResponseBody> login(@RequestParam(value = "email") String email,
                                                  @RequestParam(value = "password") String password) {
        try {
            Optional<User> user = loginService.getUserFromDb(email, password);
            User user1 = user.get();
            String jwt = loginService.createJwt(email, user1.getName(), new Date());
            return ResponseEntity.status(HttpStatus.OK).header("jwt", jwt)
                    .body(new JsonResponseBody(HttpStatus.OK.value(), "Login Successful"));
        } catch (UserNotInDatabaseException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new JsonResponseBody(HttpStatus.FORBIDDEN.value(),
                    "forbidden"));
        } catch (UnsupportedEncodingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new JsonResponseBody(HttpStatus.BAD_REQUEST.value(), "Bad Request"));
        }
    }

    @GetMapping("/showToDos")
    public ResponseEntity<JsonResponseBody> showToDos(HttpServletRequest request) {
        try {
            Map<String, Object> userData = loginService.verifyJwtAndGetData(request);
            return ResponseEntity.status(HttpStatus.OK).body(new JsonResponseBody(HttpStatus.OK.value(),
                    toDoService.getToDos((String) userData.get("email"))));
        } catch (UnsupportedEncodingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new JsonResponseBody(HttpStatus.BAD_REQUEST.value(), "Bad Request " + e.getMessage()));
        } catch (UserNotAuthenticatedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    new JsonResponseBody(HttpStatus.FORBIDDEN.value(), "Forbidden " + e.getMessage()));
        } catch (ExpiredJwtException e) {
            return ResponseEntity.status(HttpStatus.GATEWAY_TIMEOUT).body(
                    new JsonResponseBody(HttpStatus.GATEWAY_TIMEOUT.value(),
                            "Gateway Timeout " + e.getMessage()));
        }
    }

    @PostMapping(value = "/createToDo")
    public ResponseEntity<JsonResponseBody> createNewToDo(HttpServletRequest request, @Valid ToDo toDo,
                                                          BindingResult result) {
        ToDoValidator validator = new ToDoValidator();
        validator.validate(toDo, result);

        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new JsonResponseBody(HttpStatus.BAD_REQUEST.value(),
                            "Data Not Valid: " + result.toString()));
        }

        try {
            loginService.verifyJwtAndGetData(request);
            return ResponseEntity.status(HttpStatus.OK).
                    body(new JsonResponseBody(HttpStatus.OK.value(), toDoService.addToDo(toDo)));
        } catch (UnsupportedEncodingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new JsonResponseBody(HttpStatus.BAD_REQUEST.value(), "Bad Request " + e.getMessage()));
        } catch (UserNotAuthenticatedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    new JsonResponseBody(HttpStatus.FORBIDDEN.value(), "Forbidden " + e.getMessage()));
        } catch (ExpiredJwtException e) {
            return ResponseEntity.status(HttpStatus.GATEWAY_TIMEOUT).body(
                    new JsonResponseBody(HttpStatus.GATEWAY_TIMEOUT.value(),
                            "Gateway Timeout " + e.getMessage()));
        }
    }

    @DeleteMapping("/deleteToDo/{id}")
    public ResponseEntity<JsonResponseBody> deleteToDo(HttpServletRequest request,
                                                       @PathVariable(name = "id") Integer toDoId) {
        try {
            loginService.verifyJwtAndGetData(request);
            toDoService.deleteToDo(toDoId);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new JsonResponseBody(HttpStatus.OK.value(), "ToDo deletion successful"));
        } catch (UnsupportedEncodingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new JsonResponseBody(HttpStatus.BAD_REQUEST.value(), "Bad Request " + e.getMessage()));
        } catch (UserNotAuthenticatedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    new JsonResponseBody(HttpStatus.FORBIDDEN.value(), "Forbidden " + e.getMessage()));
        } catch (ExpiredJwtException e) {
            return ResponseEntity.status(HttpStatus.GATEWAY_TIMEOUT).body(
                    new JsonResponseBody(HttpStatus.GATEWAY_TIMEOUT.value(),
                            "Gateway Timeout " + e.getMessage()));
        }
    }
}
