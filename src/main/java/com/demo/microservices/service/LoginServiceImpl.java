package com.demo.microservices.service;

import com.demo.microservices.dao.UserDao;
import com.demo.microservices.entities.User;
import com.demo.microservices.util.EncryptionUtils;
import com.demo.microservices.util.JwtUtils;
import com.demo.microservices.util.UserNotAuthenticatedException;
import com.demo.microservices.util.UserNotInDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserDao userDao;

    @Autowired
    EncryptionUtils encryptionUtils;

    @Autowired
    JwtUtils jwtUtils;

    @Override
    public Optional<User> getUserFromDb(String email, String password) throws UserNotInDatabaseException {
        Optional<User> user = userDao.findUserByEmail(email);
        if (user.isPresent()) {
            User user1 = user.get();
            if (! encryptionUtils.decrypt(user1.getPassword()).equals(password)) {
                throw new UserNotInDatabaseException("Wrong Email or Password");
            }
        } else {
            throw new UserNotInDatabaseException("Wrong Email or Password");
        }
        return user;
    }

    @Override
    public String createJwt(String email, String name, Date date) throws UnsupportedEncodingException {
        date.setTime(date.getTime() + (300*1000));
        return jwtUtils.generateJwt(email, name, date);
    }

    @Override
    public Map<String, Object> verifyJwtAndGetData(HttpServletRequest request) throws UnsupportedEncodingException,
            UserNotAuthenticatedException {
        String jwt = jwtUtils.getJwtFromHttpRequest(request);
        if (null == jwt) {
            throw new UserNotAuthenticatedException("User not logged in!!");
        }
        return jwtUtils.jwt2Map(jwt);
    }
}
