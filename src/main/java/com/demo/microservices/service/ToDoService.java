package com.demo.microservices.service;

import com.demo.microservices.entities.ToDo;

import java.util.List;

public interface ToDoService {

    List<ToDo> getToDos(String email);

    ToDo addToDo(ToDo toDo);

    void deleteToDo (Integer id);
}
