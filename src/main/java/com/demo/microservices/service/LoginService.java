package com.demo.microservices.service;

import com.demo.microservices.entities.User;
import com.demo.microservices.util.UserNotAuthenticatedException;
import com.demo.microservices.util.UserNotInDatabaseException;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

public interface LoginService {

    Optional<User> getUserFromDb(String email, String password) throws UserNotInDatabaseException;

    String createJwt(String email, String name, Date date) throws UnsupportedEncodingException;

    Map<String, Object> verifyJwtAndGetData(HttpServletRequest request) throws UnsupportedEncodingException,
            UserNotAuthenticatedException;
}
